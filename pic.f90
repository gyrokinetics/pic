module fft
  
  implicit none
  
  integer fftw_forward, fftw_backward
  parameter (fftw_forward=-1, fftw_backward=1)
  
  integer fftw_real_to_complex, fftw_complex_to_real
  parameter (fftw_real_to_complex=-1, fftw_complex_to_real=1)
  
  integer fftw_estimate, fftw_measure
  parameter (fftw_estimate=0, fftw_measure=1)
  
  integer fftw_out_of_place,fftw_in_place
  parameter (fftw_out_of_place=0, fftw_in_place=8)
  
end module fft

!-------------------------------------------------------------------------------------------------------------------------------------

program PIC_GK

  implicit none

! On Dawson, one should link this program with the command
! xlf90 pic.f90 -qsuffix=f=f90 -L/u/home2/nfl/FFTW/lib -lrfftw -lfftw
! On MRF
! ifort pic.f90 -L/u/home2/nfl/FFTW/lib -lrfftw -lfftw
! On bassi
! xlf90_r pic.f90 -o pic.x -bmaxdata:0x80000000 -bmaxstack:0x80000000 -L/usr/common/usg/fftw/2.1.5/include 
! -L/usr/common/usg/fftw/2.1.5/lib -ldfftw -ldrfftw


!--------------------constants-----------------------------
  integer, parameter :: pr = 8
  complex, parameter :: zi = (0.0,1.0)
  real(kind=pr), parameter :: pi = 3.1415926535897931
!----------------------------------------------------------

!-------------------------------------------------------------program variables-------------------------------------------------------
  integer :: ikx, iky, ikz, ikvperp, step
  integer :: i, j, k, l
  
  
  real(kind=pr) :: Volume_scale
  real(kind=pr) :: delta_x, delta_y, delta_z
  
  real(kind=pr), dimension(:), allocatable :: x_grid, y_grid, z_grid
  
  real(kind=pr), dimension (:),   allocatable :: kx, ky, kz
  real(kind=pr), dimension (:,:), allocatable :: k2_perp
  
  complex(kind=pr), dimension (:,:,:), allocatable :: phi
  complex(kind=pr), dimension (:,:,:), allocatable :: phi_old 
  
  real (kind=pr), target, dimension(:,:), allocatable :: p   ! particle array
  
  real(kind=pr), dimension (:), allocatable :: vperp, vperp_weight  
  
  real(kind=pr) :: J0_init
  real(kind=pr), dimension(:,:,:), allocatable :: J0
  
  real(kind=pr) :: scale_x, scale_y, scale_z

  real(kind=pr) :: velx_max, vely_max, velx_stepsize, vely_stepsize, vel

  real(kind=pr), dimension(:), allocatable :: xpos_true, ypos_true, zpos_true

  real(kind=pr), dimension(:), pointer :: xpos, ypos, zpos, wpos
  real(kind=pr), dimension(:), pointer :: xmid, ymid, zmid, wmid
  real(kind=pr), dimension(:), pointer :: velx, vely, Epar, wdot
  real(kind=pr), dimension(:), pointer :: velz, vzw,  velp, vpw

! Indices for the particle arrays
  integer, parameter :: x  = 1,  y = 2,  z  = 3,  wgt  = 4
  integer, parameter :: xm = 5, ym = 6,  zm = 7,  wgtm = 8
  integer, parameter :: vx = 9, vy = 10, vz = 11, Ez = 12, zw = 13
  integer, parameter :: vp = 14, pw = 15

! NOTE: wdot and Epar are different items which will share the same memory
! because we do not need both at the same time.
  
  integer :: spectrum_k2_unit, spectrum_kx_unit, spectrum_ky_unit, phi2d_x_unit, phi3d_x_unit, energy_unit
  integer :: diagnostics_1_unit, diagnostics_2_unit, diagnostics_3_unit, diagnostics_4_unit
  integer :: diagnostics_traj_unit
  integer :: restart_unit
  integer :: vel_diagnostics_unit

  character(len=40) :: phi_2_str, rho_2_str, rho_x_str, log_phi_2_str
  character(len=40) :: spectrum_k2_str, spectrum_kx_str, spectrum_ky_str, phi2d_x_str, phi3d_x_str, energy_str
  character(len=40) :: diagnostics_1_str, diagnostics_2_str, diagnostics_3_str, diagnostics_4_str
  character(len=40) :: diagnostics_traj_str
  character(len=40) :: restart_str
  character(len=40) :: vel_diagnostics_str

  integer, dimension (3) :: Msize
  
  real :: time
  
  integer, dimension (:), allocatable :: Nvz_Nvperp
  integer :: Number_Chi, Number_E, Number_Chi_total
  integer, dimension(:), allocatable :: Energy_int, Chi_int

  character (len=10) :: zdate, ztime, zzone
  integer, dimension(8) :: ival
  real :: time_start, time_end

  logical :: first_nonlinear = .TRUE.

!--------------------------------------------------------------------------------------------------------------------------------------


!-------------------------------------------------------------input parameters---------------------------------------------------------

  integer :: in_unit=60
  character(len=40) :: runname
  character(len=40) :: in_str

  ! number of spatial gridpoints (Nx,Ny; must be divisible by 4) and box length (Lx,Ly)
  integer       :: Nx=32, Ny=32, Nz=32
  real(kind=pr) :: Lx=64.0, Ly=64.0, Lz=100.0   

 ! velocity grid
  integer :: Nvz 
  integer :: Nvperp = 8
  real(kind=pr) :: dvperp = 0.1

 ! number of particles 
  integer       :: Ncell=100  ! Number of particles at some vperp, vpar in original coding.
  integer       :: N  ! Total = Ncell * Nvperp * Nvz; number of total particles

! delta_t is the time step, has to be made adaptive 
  real(kind=pr) :: delta_t = 0.01

! number of time steps
  integer :: nstep = 100

! scale factor for initial weights
  real(kind=pr) :: factor_delta = 1.

! tau escribes the ratio of T_{i} to T_{e} ... see Normalization.pdf
  real(kind=pr) :: tau = 1.

! charge is the number of charges carried by one ion
  integer :: charge = 1

! A length scale in parallel direction
! Ln is normalized to A and describes the density gradient scale length in the x-direction n(x) = n_{0} exp(-x/Ln)
  real(kind=pr) :: Ln = 1., nprime
! LT is normalized to A and describes the ion temperature gradient scale length in the x-direction T(x) = T_{0} exp(-x/LT)
  real(kind=pr) :: LT = 1., tprime

! for trajectory diagnostics
  integer :: Ndisp          = 5000
  integer :: Nvbin          = 50
  integer :: traj_dia_step  = 5
  real(kind=pr) :: velxx_stepsize 
  real(kind=pr) :: velyy_stepsize
  real(kind=pr) :: velxy_stepsize

! flags managing to turn on the diagnostics and within the diagnostics certain parts
  logical :: diagnostics_on=.FALSE.
  logical :: spectrum_diagnostics_on=.FALSE.
  logical :: phi2d_dia_on=.FALSE.
  logical :: phi3d_dia_on=.FALSE.
  logical :: energy_diagnostics_on=.FALSE.

  logical :: diagnostics_new_on=.FALSE.
  logical :: diagnostics_1_on  =.FALSE.
  logical :: diagnostics_2_on  =.FALSE.
  logical :: diagnostics_3_on  =.FALSE.
  logical :: diagnostics_4_on  =.FALSE.
  logical :: diagnostics_traj_on =.FALSE.

  logical :: save_for_restart =.FALSE.
  logical :: init_restart     =.FALSE.

  integer :: nwrite = 10
  integer :: spectrum_write = 10

  integer :: phi2d_dia_time = 10
  integer :: phi3d_dia_time = 10

  real(kind=pr) :: time_growth_off =   1.e10  ! time when linear growth gets turned OFF (tprime=nprime=0.0) 
  real(kind=pr) :: time_collision_on = 1.e10  ! time when collision gets turned ON
  real(kind=pr) :: nonlinear_phase   = 1.e10  ! time when run becomes nonlinear and time_step is reduced

! peak of the weibull distribution of the perp velocity. 
  real(kind=pr) :: peak_velocity_perp = 1.

! to turn on the update of the particle positions in x and y; if positions are updated the run is nonlinear
  logical :: nonlinear_on =.TRUE.

! to include parallel nonlinearity -- need to revisit normalizations if this is true?
  logical :: ParkerLee = .FALSE.
  logical :: finishing = .false.

! parameters for the collision operator
  integer :: N_Chi = 4 
  real(kind=pr) :: dE= 0.3
  real(kind=pr) :: gamma = 0.1    ! "diffusion-parameter" added 04/15/08
  logical :: collision_on =.FALSE.
  integer :: collision_time = 10

! parameters for velocity_space diagnostics
  integer :: v_dia_x = 1
  integer :: v_dia_y = 1
  integer :: v_dia_z = 1
  integer :: velocity_space_diagnostics_time = 50
  logical :: velocity_space_diagnostics_on = .FALSE.

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!! Begin executable code !!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  call date_and_time (zdate, ztime, zzone, ival)
  time_start = ival(5)*3600.+ival(6)*60.+ival(7)+ival(8)/1000.
  write(*,*) "Code starts running at :", time_start

  call read_inputs

  call setup_IO

  allocate(Nvz_Nvperp(0:Nvperp))
  Nvz_Nvperp(0) = 0

  N = 0 
  do i =1,Nvperp
     Nvz_Nvperp(i)=Ncell*SQRT((Nvperp+1)**2.-i**2.)
     Nvz = Nvz_Nvperp(i)
     do j=1,Nvz
        N=N+1
     end do
  end do

  ! check: the sample size at a value of vperp cannot 
  ! be larger than the block size
  if(Ndisp>Nvz_Nvperp(Nvperp)) then 
     write(*,*) " Ndisp is too large for Nvperp "
  end if

  call allocate_memory

  call set_grid_parameters

  time = 0.0

  if(init_restart) then     
     open(restart_unit,file=restart_str,status='old')
     read(restart_unit,*) time
     do i=1,N
        read(restart_unit,*) xpos(i),ypos(i),zpos(i),wpos(i),velz(i)
     end do
     close(restart_unit)
     do i=1,N
        vzw(i) = exp(-(velz(i)**2.)/2.)
     end do
  else
   
     call init_particles
   
     call init_velocity_z 

  end if     

  if(diagnostics_traj_on) then
     call traj_diagnostics(delta_t,velx,vely,velz,Nvperp,Ndisp,Nvz_Nvperp,xpos,ypos,zpos,xpos_true,ypos_true,zpos_true)
  end if

  call init_velocity_perp (vperp, dvperp)

  call init_J0 (vperp, J0)

  call interpolation_E_Chi(Energy_int,Chi_int)


!
!  Calculate phi based on the initial gyrocenter-positions
!
  call calculate_phi(wpos, xpos, ypos, zpos, phi)

  if(spectrum_diagnostics_on) then
     call spectrum_phi(phi)
  end if
         
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!                                                                       !!!!
!!!!              This is the time advancement loop                        !!!!
!!!!                                                                       !!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  write(*,*) " total number of particles = ", N

  do step = 1, nstep
     
     time = time + delta_t
     write(*,*) "time = ", time
     if (time > nonlinear_phase .AND. first_nonlinear) then
        delta_t = delta_t * 0.1
        collision_time = collision_time * 10
        velocity_space_diagnostics_time = velocity_space_diagnostics_time * 10 ! have to make this part better, right now fixed manually 
        first_nonlinear = .FALSE.
     end if
     if (time > time_collision_on) collision_on =.TRUE. 
     if (time > time_growth_off)   then 
        tprime=0.0
        nprime=0.0
     end if
     
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!                                                                       !!!!
!!!!  First take a half time step (moving particles and updating weights)  !!!!
!!!!                                                                       !!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!
!    calculate electric field components for each particle
!
     call calculate_electric_field(phi, xpos, ypos, zpos, velx, vely, Epar)

!
!    move the gyrocenters to mid-point: 
!
     if (nonlinear_on) then  ! linear case is okay.  
        do i=1,N
           xmid(i) = modulo(xpos(i) + velx(i) * 0.5 * delta_t, Lx)
           ymid(i) = modulo(ypos(i) + vely(i) * 0.5 * delta_t, Ly)
        end do
     end if
     do i=1,N
        zmid(i) = modulo(zpos(i) + velz(i) * 0.5 * delta_t, Lz)
     end do

!
!  Find weights at mid-point
!
     call update_weights(wpos, wdot, velz, vperp, velx, Epar)
     do i=1,N
        wmid(i) = wpos(i) + wdot(i) * 0.5 * delta_t  
     end do

     if (mod(step,collision_time)==0 .AND. collision_on) then
        call collision(wmid, xmid, ymid, zmid,Energy_int,Chi_int)
     end if

!
!  Find fields at mid-point
!

     call calculate_phi(wmid, xmid, ymid, zmid, phi)        

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!                                                                       !!!!
!!!!  Then take a full time step (moving particles and updating weights)   !!!!
!!!!                                                                       !!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

     call calculate_electric_field(phi, xmid, ymid, zmid, velx, vely, Epar)
     
     if (nonlinear_on) then
        do i=1,N
           xpos(i) = modulo(xpos(i) + velx(i) * delta_t, Lx)
           ypos(i) = modulo(ypos(i) + vely(i) * delta_t, Ly)
        end do
     end if
     do i=1,N
        zpos(i) = modulo(zpos(i) + velz(i) * delta_t, Lz)
     end do

     call update_weights(wmid, wdot, velz, vperp, velx, Epar)
     do i=1,N
        wpos(i) = wpos(i) + wdot(i) * delta_t  
     end do

     if(diagnostics_traj_on) then
        call traj_diagnostics(delta_t,velx,vely,velz,Nvperp,Ndisp,Nvz_Nvperp,xpos,ypos,zpos,xpos_true,ypos_true,zpos_true)
     end if

     if (mod(step,collision_time)==0 .AND. collision_on) then
        call collision(wpos, xpos, ypos, zpos,Energy_int,Chi_int)
     end if

!
!  Find fields from new particle positions
!
     call calculate_phi(wpos, xpos, ypos, zpos, phi)        

     if(mod(step,spectrum_write)==0 .AND. spectrum_diagnostics_on) then        
        call spectrum_phi(phi)
     end if
     
     if (diagnostics_new_on .and. mod(step,nwrite)==0) then
        call diagnostics_new(phi, phi_old)
     end if
     
     if (mod(step,phi3d_dia_time)==0 .AND. phi3d_dia_on) then
        call phi_diagnostics(phi)
     end if

  end do
!----------------------------------------------END time advance loop--------------------------------------------------------

  finishing = .true.
  call diagnostics_new (phi, phi_old, finishing)

  if(diagnostics_new_on) then
     if(diagnostics_1_on) close(diagnostics_1_unit)
     if(diagnostics_2_on) close(diagnostics_2_unit)
     if(diagnostics_3_on) close(diagnostics_3_unit)
     if(diagnostics_4_on) close(diagnostics_4_unit)
  end if

  if (diagnostics_traj_on) then
     close(diagnostics_traj_unit)
  end if

  if (spectrum_diagnostics_on) then
     close(spectrum_k2_unit)
     close(spectrum_kx_unit)
     close(spectrum_ky_unit)
  end if

  if (velocity_space_diagnostics_on) then
     close(vel_diagnostics_unit)
  end if

  if (phi2d_dia_on) then
     close(phi2d_x_unit)
  end if

  if (phi3d_dia_on) then
     close(phi3d_x_unit)
  end if

  if (save_for_restart) then
     open(restart_unit, file=restart_str,status='replace')
     write(restart_unit,*) time
     do i=1,N
        write(restart_unit,*) xpos(i), ypos(i), zpos(i), wpos(i), velz(i)
     end do
     close(restart_unit)
  end if

  call date_and_time (zdate, ztime, zzone, ival)
  time_end = ival(5)*3600.+ival(6)*60.+ival(7)+ival(8)/1000.
  write(*,*) "Code stops running at :", time_end
  write(*,*) "Total run time :", time_end-time_start

contains

  subroutine diagnostics_new(phi_in, phi_old, finishing)
       
    use fft
    logical, save :: first = .true.
    integer :: i, j, k, idx

    complex(kind=pr), dimension(0:,0:,0:), intent(in) :: phi_in
    logical, optional :: finishing
    complex(kind=pr), dimension(0:,0:,0:), intent(inout) :: phi_old

    logical :: writemore

    complex(kind=pr)  :: omega
    real :: kx_max, ky_max, kz_max, phi2_max
    complex(kind=pr), dimension(:,:,:), allocatable :: phi_new

    real(kind=pr) :: E_phi ! see definition Denton Kotschenreuther (1.19) f.f.
    real(kind=pr) :: sum_weights, heat_flux
    allocate(phi_new(0:Nx/2,0:Ny-1,0:Nz-1))

    E_phi = 0.5*SUM(phi_in*Conjg(phi_in))

    writemore = .false.
    if (present(finishing)) writemore = finishing

!    write(diagnostics_1_unit,*) time, E_phi, real(phi_dia(Nx/4,Ny/2,Nz/2)), aimag(phi_dia(Nx/4,Ny/2,Nz/2))
!    write(*,*) time, E_phi!, real(phi(Nx/4,Ny/2,Nz/2)), aimag(phi(Nx/4,Ny/2,Nz/2))

!    if (writemore) then
!       do k = 0,Nz-1
!          do j = 0,Ny-1
!             do i = 0,Nx/2
!                write(diagnostics_2_unit,100) kx(i),ky(j),kz(k),real(phi(i,j,k)), aimag(phi(i,j,k)), real(phi(i,j,k)*conjg(phi(i,j,k)))
!             end do
!          end do
!          write (diagnostics_2_unit,*)
!       end do
!       write (diagnostics_2_unit,*)
!       write (diagnostics_2_unit,*)
!    end if

100 format(6(1x,es12.4))

    phi2_max = 1.e-25
    kx_max = 0.
    ky_max = 0.
    kz_max = 0.

    do ikz=0,Nz/2
       do iky=0,Ny/2
          do ikx=0,Nx/2
             if (REAL(phi_in(ikx,iky,ikz)*Conjg(phi_in(ikx,iky,ikz))) > phi2_max) then
                omega = (zi/(nwrite*delta_t))*log(phi_in(ikx,iky,ikz)/phi_old(ikx,iky,ikz))
                phi2_max = phi(ikx,iky,ikz)*Conjg(phi_in(ikx,iky,ikz))
                kx_max = ikx
                ky_max = iky
                kz_max = ikz
             end if
          end do
       end do
    end do

    write(diagnostics_3_unit,100) real(omega), aimag(omega), kx_max, ky_max, kz_max

    phi_old        = phi_in
!    phi_old(1,1,1) = phi(1,1,1) * 1e-8
   
    idx = 0
    do i = 1,Nvperp
       Nvz = Nvz_Nvperp(i)
       do j = 1,Nvz
          idx = idx + 1
          heat_flux = heat_flux + (vperp(i)**2. + velz(idx)**2.)*wpos(idx)*velx(idx)
          sum_weights = sum_weights + wpos(idx)**2.
       end do
    end do

    heat_flux = heat_flux*(1./real(N))*0.5

    sum_weights = sum_weights/real(N)
    write(diagnostics_4_unit,100) time, sum_weights, heat_flux, E_phi

    if(MOD(step,phi2d_dia_time)==0 .AND. phi2d_dia_on) then
       call phi_diagnostics (phi_in)
    end if

  end subroutine diagnostics_new

!----------------------------------------------------------------------------------------------------------
  subroutine velocity_space_diagnostics(delta_f_in)

    real(kind=pr), dimension(:,:), intent(in) :: delta_f_in
    
    real(kind=pr), dimension(:), allocatable :: vperp_grid, vz_grid

    integer :: idx, Num_vz
    logical, save :: first_vel_dia = .TRUE.

    allocate(vperp_grid(1:Number_Chi_total))
    allocate(vz_grid(1:Number_Chi_total))

    if (first_vel_dia) then
       open(unit=vel_diagnostics_unit, file=vel_diagnostics_str, status='replace')
       first_vel_dia = .FALSE.
    end if

    idx = 0 

    do i =1,Number_E
       Num_vz = N_Chi*(2*i-1)
       do j=1,Num_vz
          idx=idx+1
          vperp_grid(idx) = SIN((2*j-1)*pi/(2.*Num_vz))*dE*(2.*i-1.)
          vz_grid(idx)    = COS((2*j-1)*pi/(2.*Num_vz))*dE*(2.*i-1.)
          write(vel_diagnostics_unit,*) vperp_grid(idx), vz_grid(idx), delta_f_in(i,j)
       end do
    end do

    idx = 0

    deallocate(vz_grid, vperp_grid)

  end subroutine velocity_space_diagnostics
!----------------------------------------------------------------------------------------------------------
  subroutine traj_diagnostics(delta_t,velx,vely,velz,Nvperp,Ndisp,Nvz_Nvperp,xpos,ypos,zpos,xpos_true,ypos_true,zpos_true)

    real(kind=pr), dimension(:), intent(in) :: velx,vely,velz,xpos,ypos,zpos
    real(kind=pr), dimension(:), intent(inout) :: xpos_true,ypos_true,zpos_true
    real(kind=pr), intent(in) :: delta_t
    integer, intent(in) :: Nvperp,Ndisp
    integer, dimension(0:), intent(in) :: Nvz_Nvperp

    integer :: start,l
    logical, save :: first_traj_dia = .TRUE.

900     format(6(2x,es16.4))

    start = 0
    l = 0
    if (first_traj_dia) then
       open(unit=diagnostics_traj_unit, file=diagnostics_traj_str, status='replace')
       do i=1,Nvperp
          start = Nvz_Nvperp(i-1) + start
          do j=1,Ndisp
             k=(i-1)*Ndisp+j
             l=start+j
             xpos_true(k) = xpos(l)
             ypos_true(k) = ypos(l)
             zpos_true(k) = zpos(l)
          end do
       end do
       do i=1,Ndisp*Nvperp
          write(diagnostics_traj_unit,900) xpos_true(i), ypos_true(i), zpos_true(i)
       end do
       first_traj_dia = .FALSE.
    else
       do i=1,Nvperp ! go through all vperp blocks
          start = Nvz_Nvperp(i-1) + start ! keep track of position on vperp ladder
          do j=1,Ndisp ! Ndisp is the size of the chunk taken from each vperp block for trajectory analysis
             k = (i-1)*Ndisp+j ! index of the subsample for output
             l = start+j ! index of the full array in velx
             xpos_true(k) = xpos_true(k) + delta_t * velx(l) ! update the trajectory locations
             ypos_true(k) = ypos_true(k) + delta_t * vely(l) ! ditto
             zpos_true(k) = zpos_true(k) + delta_t * velz(l) ! ditto
             if(MOD(step,traj_dia_step)==0) then ! output at certain intervals
                write(diagnostics_traj_unit,900) xpos_true(k), ypos_true(k), zpos_true(k)
             end if
          end do
       end do
    end if

  end subroutine traj_diagnostics
!------------------------------------------

  subroutine calculate_phi(weight, x_gc, y_gc, z_gc, phi)
    
    use fft
    
    real(kind=pr), dimension(:), intent (in) :: weight
    real(kind=pr), dimension(:), intent (in) :: x_gc
    real(kind=pr), dimension(:), intent (in) :: y_gc
    real(kind=pr), dimension(:), intent (in) :: z_gc
    complex(kind=pr), dimension(0:,0:,0:), intent (out) :: phi
    
    real(kind=pr) :: g1, g2, g3, g4, g5, g6, g7, g8
    real(kind=pr), dimension(:,:,:), allocatable :: grid, gnrm
    complex(kind=pr), dimension(:,:,:), allocatable :: rho_k
    real(kind=pr) :: dennorm, dnorm
    
    real(kind=pr), dimension(:,:), allocatable :: Gamma_0
    
    complex(kind=pr), dimension(:,:,:), allocatable :: J0_rho_k
    
    real (kind=pr) :: fac

    integer :: idx, L0, L1, L2, M0, M1, M2, N0, N1, N2
    integer :: ivperp, ivz, icell
    integer*8, save :: plan_real2compl
    logical, save :: beginning=.true.
    integer, save :: phi_subroutine_count = 0

    allocate (grid(0:Nx-1,0:Ny-1,0:Nz-1)) 
    allocate (gnrm(0:Nx-1,0:Ny-1,0:Nz-1)) 
    allocate (rho_k(0:Nx/2,0:Ny-1,0:Nz-1))  ! no need to initialize this one.
    allocate (J0_rho_k(0:Nx/2,0:Ny-1,0:Nz-1))  ; J0_rho_k = 0.
    allocate ( Gamma_0(0:Nx/2,0:Ny-1))

    phi_subroutine_count = phi_subroutine_count + 1

    if(beginning) then
       call rfftwnd_f77_create_plan (plan_real2compl,3,Msize,fftw_real_to_complex,fftw_estimate) 
       beginning=.false.
    end if

! For each vperp value, interpolate the charge to the x,y,z grid.  Integrates over v_parallel.

    dennorm = 0.
    idx = 0 
    do ivperp = 1,Nvperp
       gnrm = 0.0
       grid = 0.0
       Nvz = Nvz_Nvperp(ivperp)
       do ivz = 1,Nvz
          idx = idx + 1
                
          L0 = x_gc(idx)/delta_x  ;  L1 = L0 + 1
          M0 = y_gc(idx)/delta_y  ;  M1 = M0 + 1
          N0 = z_gc(idx)/delta_z  ;  N1 = N0 + 1                          
                
          L2 = mod(L1, NX)  ;  M2 = mod(M1, NY) ;  N2 = mod(N1, NZ)
                
          g1 = (x_grid(L1) - x_gc(idx)) * (y_grid(M1) - y_gc(idx)) * (z_grid(N1) - z_gc(idx))
          g2 = (x_gc(idx) - x_grid(L0)) * (y_grid(M1) - y_gc(idx)) * (z_grid(N1) - z_gc(idx))
          
          g3 = (x_grid(L1) - x_gc(idx)) * (y_gc(idx) - y_grid(M0)) * (z_grid(N1) - z_gc(idx))
          g4 = (x_gc(idx) - x_grid(L0)) * (y_gc(idx) - y_grid(M0)) * (z_grid(N1) - z_gc(idx))
          
          g5 = (x_grid(L1) - x_gc(idx)) * (y_grid(M1) - y_gc(idx)) * (z_gc(idx) - z_grid(N0))
          g6 = (x_gc(idx) - x_grid(L0)) * (y_grid(M1) - y_gc(idx)) * (z_gc(idx) - z_grid(N0))
          
          g7 = (x_grid(L1) - x_gc(idx)) * (y_gc(idx) - y_grid(M0)) * (z_gc(idx) - z_grid(N0))
          g8 = (x_gc(idx) - x_grid(L0)) * (y_gc(idx) - y_grid(M0)) * (z_gc(idx) - z_grid(N0))

          fac = weight(idx)*vzw(idx)*Volume_scale
                
          grid(L0,M0,N0) = grid(L0,M0,N0) + g1*fac  ;  gnrm(L0,M0,N0) = gnrm(L0,M0,N0) + vzw(idx)*g1*Volume_scale
          grid(L2,M0,N0) = grid(L2,M0,N0) + g2*fac  ;  gnrm(L2,M0,N0) = gnrm(L2,M0,N0) + vzw(idx)*g2*Volume_scale
          
          grid(L0,M2,N0) = grid(L0,M2,N0) + g3*fac  ;  gnrm(L0,M2,N0) = gnrm(L0,M2,N0) + vzw(idx)*g3*Volume_scale
          grid(L2,M2,N0) = grid(L2,M2,N0) + g4*fac  ;  gnrm(L2,M2,N0) = gnrm(L2,M2,N0) + vzw(idx)*g4*Volume_scale
          
          grid(L0,M0,N2) = grid(L0,M0,N2) + g5*fac  ;  gnrm(L0,M0,N2) = gnrm(L0,M0,N2) + vzw(idx)*g5*Volume_scale
          grid(L2,M0,N2) = grid(L2,M0,N2) + g6*fac  ;  gnrm(L2,M0,N2) = gnrm(L2,M0,N2) + vzw(idx)*g6*Volume_scale
          
          grid(L0,M2,N2) = grid(L0,M2,N2) + g7*fac  ;  gnrm(L0,M2,N2) = gnrm(L0,M2,N2) + vzw(idx)*g7*Volume_scale
          grid(L2,M2,N2) = grid(L2,M2,N2) + g8*fac  ;  gnrm(L2,M2,N2) = gnrm(L2,M2,N2) + vzw(idx)*g8*Volume_scale
          
       end do

! Here, normalizing density in each cell

       where (abs(gnrm) > epsilon(0.0)) 
          grid = grid/gnrm   
       end where

       call rfftwnd_f77_one_real_to_complex (plan_real2compl, grid, rho_k)
       
       rho_k = rho_k/real(Nx*Ny*Nz)   ! FFT scale factor

! get contribution to integration over vperp at each kx, ky, kz:
       do ikz=0,Nz-1
          do iky=0,Ny-1
             do ikx=0,Nx/2
                J0_rho_k(ikx,iky,ikz) = J0_rho_k(ikx,iky,ikz) + J0(ikx,iky,ivperp) * rho_k(ikx,iky,ikz) * vperp_weight(ivperp)
             end do
          end do
       end do
       dennorm = dennorm + vperp_weight(ivperp)
    end do

    J0_rho_k = J0_rho_k/dennorm ! the vperp normalization

    do ikz =0,Nz-1
       do iky=0,Ny-1
          do ikx=0,Nx/2
             call gamma_n (k2_perp(ikx,iky), Gamma_0(ikx,iky))   ! calculate on the fly b/c of low GPU memory? 
             phi(ikx,iky,ikz) = J0_rho_k(ikx,iky,ikz)/(tau + charge - charge*Gamma_0(ikx,iky))
          end do
       end do
    end do
    
    phi(0,0,0) = 0.0  

!    if(MOD(step,phi_diagnostics_time)==0 .AND. MOD(phi_subroutine_count-1,2)==0) then
!       call phi_diagnostics (phi)
!    end if

    deallocate(rho_k, Gamma_0, J0_rho_k, grid, gnrm)

  end subroutine calculate_phi

!----------------------------------------------------------------------------------------------------------
  subroutine phi_diagnostics(phi_in) 
   
    use fft
    integer*8, save :: plan_comp2real
    logical, save   :: first_time_in = .true.
    complex(kind=pr), dimension(0:,0:,0:), intent (in) :: phi_in
    real(kind=pr), dimension(:,:,:), allocatable :: phi_x

    allocate(phi_x(0:Nx-1,0:Ny-1,0:Nz-1))

    if (first_time_in) then
       call rfftwnd_f77_create_plan (plan_comp2real,3,Msize,fftw_complex_to_real,fftw_estimate)
       first_time_in=.false.
    end if

    call rfftwnd_f77_one_complex_to_real (plan_comp2real, phi_in, phi_x)

    if(phi2d_dia_on) then
       ikz=10
       do iky=0,Ny-1
          do ikx=0,Nx-1
             write(phi2d_x_unit,*) ikx*delta_x, iky*delta_y, ikz*delta_z, phi_x(ikx,iky,ikz)
          end do
       end do
    end if

    if(phi3d_dia_on) then
       do ikz=0,Nz-1
          do iky=0,Ny-1
             do ikx=0,Nx-1
                write(phi3d_x_unit,*) ikx*delta_x, iky*delta_y, ikz*delta_z, phi_x(ikx,iky,ikz)
             end do
          end do
       end do
    end if

    deallocate(phi_x)

  end subroutine phi_diagnostics

!----------------------------------------------------------------------------------------------------------
  subroutine calculate_electric_field(phi, x_gc, y_gc, z_gc, vx_gc, vy_gc, Epar_gc)
    
    use fft
    
    integer :: idx
    integer*8, save :: plan_k2real
    logical, save :: first=.true.
    
    complex(kind=pr), dimension(0:,0:,0:), intent (in) :: phi
    real(kind=pr), dimension(:), intent (in) :: x_gc, y_gc, z_gc
    real(kind=pr), dimension(:), intent (out) :: vx_gc, vy_gc, Epar_gc
    
    complex(kind=pr), dimension(:,:,:), allocatable :: J0_Ex_k, J0_Ey_k, J0_Ez_k
    real   (kind=pr), dimension(:,:,:), allocatable :: Ex_x, Ey_x, Ez_x

    integer :: L0, L1, L2, M0, M1, M2, N0, N1, N2
    
    allocate(J0_Ex_k(0:Nx/2,0:Ny-1,0:Nz-1))
    allocate(J0_Ey_k(0:Nx/2,0:Ny-1,0:Nz-1))
    allocate(J0_Ez_k(0:Nx/2,0:Ny-1,0:Nz-1))
    
    allocate(Ex_x(0:Nx-1,0:Ny-1,0:Nz-1))
    allocate(Ey_x(0:Nx-1,0:Ny-1,0:Nz-1))
    allocate(Ez_x(0:Nx-1,0:Ny-1,0:Nz-1))

    if (first) then
       call rfftwnd_f77_create_plan (plan_k2real,3,Msize,fftw_complex_to_real,fftw_estimate)
       first=.false.
    end if
    
    idx = 0
    do ikvperp=1,Nvperp  ! for each vperp value...
       do ikz=0,Nz-1
          do iky=0,Ny-1
             do ikx=0,Nx/2
                J0_Ex_k(ikx,iky,ikz) = J0(ikx,iky,ikvperp) * (-zi*kx(ikx)*phi(ikx,iky,ikz))
                J0_Ey_k(ikx,iky,ikz) = J0(ikx,iky,ikvperp) * (-zi*ky(iky)*phi(ikx,iky,ikz))
                J0_Ez_k(ikx,iky,ikz) = J0(ikx,iky,ikvperp) * (-zi*kz(ikz)*phi(ikx,iky,ikz))
             end do
          end do
       end do
       
       call rfftwnd_f77_one_complex_to_real (plan_k2real, J0_Ex_k, Ex_x)
       call rfftwnd_f77_one_complex_to_real (plan_k2real, J0_Ey_k, Ey_x)
       call rfftwnd_f77_one_complex_to_real (plan_k2real, J0_Ez_k, Ez_x)
       
       Nvz = Nvz_Nvperp(ikvperp)
       do k=1,Nvz
          
          idx = idx + 1
          
          L0 = (x_gc(idx)/delta_x)  ;  L1 = L0 + 1
          M0 = (y_gc(idx)/delta_y)  ;  M1 = M0 + 1
          N0 = (z_gc(idx)/delta_z)  ;  N1 = N0 + 1
          
          L2 = mod(L1, NX)  ;  M2 = mod(M1, NY) ;  N2 = mod(N1, NZ)
          
          vx_gc(idx) = Volume_scale*( &
               (Ey_x(L0,M0,N0)*(x_grid(L1) - x_gc(idx))*(y_grid(M1) - y_gc(idx))* (z_grid(N1) - z_gc(idx))) + &
               (Ey_x(L2,M0,N0)*(x_gc(idx) - x_grid(L0))*(y_grid(M1) - y_gc(idx))* (z_grid(N1) - z_gc(idx))) + &

               (Ey_x(L0,M2,N0)*(x_grid(L1) - x_gc(idx))*(y_gc(idx) - y_grid(M0))* (z_grid(N1) - z_gc(idx))) + &
               (Ey_x(L2,M2,N0)*(x_gc(idx) - x_grid(L0))*(y_gc(idx) - y_grid(M0))* (z_grid(N1) - z_gc(idx))) + &

               (Ey_x(L0,M0,N2)*(x_grid(L1) - x_gc(idx))*(y_grid(M1) - y_gc(idx))* (z_gc(idx) - z_grid(N0))) + &
               (Ey_x(L2,M0,N2)*(x_gc(idx) - x_grid(L0))*(y_grid(M1) - y_gc(idx))* (z_gc(idx) - z_grid(N0))) + &

               (Ey_x(L0,M2,N2)*(x_grid(L1) - x_gc(idx))*(y_gc(idx) - y_grid(M0))* (z_gc(idx) - z_grid(N0))) + &
               (Ey_x(L2,M2,N2)*(x_gc(idx) - x_grid(L0))*(y_gc(idx) - y_grid(M0))* (z_gc(idx) - z_grid(N0))) )
          
          vy_gc(idx) = -Volume_scale*( &  ! minus b/c of cross product
               (Ex_x(L0,M0,N0)*(x_grid(L1) - x_gc(idx))*(y_grid(M1) - y_gc(idx))* (z_grid(N1) - z_gc(idx))) + &
               (Ex_x(L2,M0,N0)*(x_gc(idx) - x_grid(L0))*(y_grid(M1) - y_gc(idx))* (z_grid(N1) - z_gc(idx))) + &

               (Ex_x(L0,M2,N0)*(x_grid(L1) - x_gc(idx))*(y_gc(idx) - y_grid(M0))* (z_grid(N1) - z_gc(idx))) + &
               (Ex_x(L2,M2,N0)*(x_gc(idx) - x_grid(L0))*(y_gc(idx) - y_grid(M0))* (z_grid(N1) - z_gc(idx))) + &

               (Ex_x(L0,M0,N2)*(x_grid(L1) - x_gc(idx))*(y_grid(M1) - y_gc(idx))* (z_gc(idx) - z_grid(N0))) + &
               (Ex_x(L2,M0,N2)*(x_gc(idx) - x_grid(L0))*(y_grid(M1) - y_gc(idx))* (z_gc(idx) - z_grid(N0))) + &

               (Ex_x(L0,M2,N2)*(x_grid(L1) - x_gc(idx))*(y_gc(idx) - y_grid(M0))* (z_gc(idx) - z_grid(N0))) + &
               (Ex_x(L2,M2,N2)*(x_gc(idx) - x_grid(L0))*(y_gc(idx) - y_grid(M0))* (z_gc(idx) - z_grid(N0))) )
          
          Epar_gc(idx) = Volume_scale*( &
               (Ez_x(L0,M0,N0)*(x_grid(L1) - x_gc(idx))*(y_grid(M1) - y_gc(idx))* (z_grid(N1) - z_gc(idx))) + &
               (Ez_x(L2,M0,N0)*(x_gc(idx) - x_grid(L0))*(y_grid(M1) - y_gc(idx))* (z_grid(N1) - z_gc(idx))) + &

               (Ez_x(L0,M2,N0)*(x_grid(L1) - x_gc(idx))*(y_gc(idx) - y_grid(M0))* (z_grid(N1) - z_gc(idx))) + &
               (Ez_x(L2,M2,N0)*(x_gc(idx) - x_grid(L0))*(y_gc(idx) - y_grid(M0))* (z_grid(N1) - z_gc(idx))) + &

               (Ez_x(L0,M0,N2)*(x_grid(L1) - x_gc(idx))*(y_grid(M1) - y_gc(idx))* (z_gc(idx) - z_grid(N0))) + &
               (Ez_x(L2,M0,N2)*(x_gc(idx) - x_grid(L0))*(y_grid(M1) - y_gc(idx))* (z_gc(idx) - z_grid(N0))) + &

               (Ez_x(L0,M2,N2)*(x_grid(L1) - x_gc(idx))*(y_gc(idx) - y_grid(M0))* (z_gc(idx) - z_grid(N0))) + &
               (Ez_x(L2,M2,N2)*(x_gc(idx) - x_grid(L0))*(y_gc(idx) - y_grid(M0))* (z_gc(idx) - z_grid(N0))) ) 
          
       end do
    end do
    
    deallocate(Ex_x, Ey_x, Ez_x, J0_Ex_k, J0_Ey_k, J0_Ez_k)
    
  end subroutine calculate_electric_field

!---------------------------------------------------------------------------------------------------------
  subroutine update_weights(w, wdot, velz, vperp, vx, Epar)

    real(kind=pr), dimension(:), intent(in) :: w, velz, vperp, vx, Epar
    real(kind=pr), dimension(:), intent(out) :: wdot
    real(kind=pr) :: fac_2 = 0., v2
    logical, save :: first = .true.
    integer :: idx

    if (first) then
       first = .false.
       if (ParkerLee) fac_2 = 1.
       nprime = -1./Ln
       tprime = -1./LT
    end if

    idx=0
    do i=1,Nvperp
       Nvz=Nvz_Nvperp(i)
       do j=1,Nvz
          idx=idx+1
          v2 = vperp(i)**2 + velz(idx)**2 
          wdot(idx) = -(1. - fac_2 * w(idx)) * (vx(idx) * (nprime+tprime *(0.5 * v2 - 1.5)) - charge * velz(idx) * Epar(idx))
       end do
    end do

  end subroutine update_weights
  

!----------------------------------------------------------------------------------------------------------

  subroutine interpolation_E_Chi(Energy_int,Chi_int)

    integer, dimension(:), intent(out) :: Energy_int
    integer, dimension(:), intent(out) :: Chi_int

    real(kind=pr) :: particle_Energy
    integer :: particle_Chi
    integer :: idx

    Number_E = ((Nvperp+1)*dvperp)/(2.*dE) ! Number of Energy grid-points (Energy-circles), MAKE THIS BETTER, otherwise wrong input might lead to errors

    Number_Chi = N_Chi*(2*Number_E-1)
    Number_Chi_total = 0 
    do i=1,Number_E
       Number_Chi_total = Number_Chi_total + N_Chi*(2*i-1) ! total number of Chi-grid-points
    end do

    write(*,*) " total number of grid points (5D) for the collision operator = ",Nx*Ny*Nz*Number_Chi_total
    write(*,*) " number of particles per 5D-E,Chi-grid = ", (8*N)/(Nx*Ny*Nz*Number_Chi_total)

    idx = 0 
    do i=1,Nvperp
       Nvz=Nvz_Nvperp(i)
       do j=1,Nvz
          idx = idx + 1
          particle_Energy = SQRT(vperp(i)**2. + velz(idx)**2.)
          Energy_int(idx) = 1 + particle_Energy/(2.*dE)
          particle_Chi = (2 * Energy_int(idx) - 1)*N_Chi
          if (velz(idx) > 0.) then
             Chi_int(idx) = 1. + (particle_Chi/pi) * ATAN(vperp(i)/velz(idx))
          else
             Chi_int(idx) = 1. + (particle_Chi/pi) * (ATAN(vperp(i)/velz(idx))+pi)
          end if
       end do
    end do

  end subroutine interpolation_E_Chi

!----------------------------------------------------------------------------------------------------------

  subroutine collision (weight, x_gc, y_gc, z_gc,Energy_int,Chi_int)

    real(kind=pr), dimension(:), intent (inout) :: weight
    real(kind=pr), dimension(:), intent (in) :: x_gc
    real(kind=pr), dimension(:), intent (in) :: y_gc
    real(kind=pr), dimension(:), intent (in) :: z_gc
    integer, dimension(:), intent (in) :: Energy_int
    integer, dimension(:), intent (in) :: Chi_int

    real(kind=pr), dimension(:,:,:,:,:), allocatable :: delta_f
    real(kind=pr), dimension(:,:,:,:,:), allocatable :: gnrm
    real(kind=pr), dimension(:,:)      , allocatable :: delta_f_dia
    
    integer :: E, Chi, idx, ivperp, ivz
    integer :: L0, L1, L2, M0, M1, M2, N0, N1, N2    
    real(kind=pr) :: g1, g2, g3, g4, g5, g6, g7, g8
    real(kind=pr) :: fac

    integer, save :: collision_count = 0 

    allocate(delta_f(0:Nx-1,0:Ny-1,0:Nz-1,Number_E,Number_Chi))
    allocate(gnrm   (0:Nx-1,0:Ny-1,0:Nz-1,Number_E,Number_Chi))
    allocate(delta_f_dia(1:Number_E,1:Number_Chi))

    collision_count = collision_count + 1 

!--- 1. interpolation to generate delta_f(x,y,z,E,Chi) ------------------------ 

    idx = 0
    delta_f = 0.0
    gnrm    = 0.0
    do ivperp = 1,Nvperp
       Nvz = Nvz_Nvperp(ivperp)

       do ivz = 1,Nvz
          idx = idx + 1
                
          L0 = x_gc(idx)/delta_x  ;  L1 = L0 + 1
          M0 = y_gc(idx)/delta_y  ;  M1 = M0 + 1
          N0 = z_gc(idx)/delta_z  ;  N1 = N0 + 1                          
                
          L2 = mod(L1, NX)  ;  M2 = mod(M1, NY) ;  N2 = mod(N1, NZ)
                
          g1 = (x_grid(L1) - x_gc(idx)) * (y_grid(M1) - y_gc(idx)) * (z_grid(N1) - z_gc(idx))
          g2 = (x_gc(idx) - x_grid(L0)) * (y_grid(M1) - y_gc(idx)) * (z_grid(N1) - z_gc(idx))
          
          g3 = (x_grid(L1) - x_gc(idx)) * (y_gc(idx) - y_grid(M0)) * (z_grid(N1) - z_gc(idx))
          g4 = (x_gc(idx) - x_grid(L0)) * (y_gc(idx) - y_grid(M0)) * (z_grid(N1) - z_gc(idx))
          
          g5 = (x_grid(L1) - x_gc(idx)) * (y_grid(M1) - y_gc(idx)) * (z_gc(idx) - z_grid(N0))
          g6 = (x_gc(idx) - x_grid(L0)) * (y_grid(M1) - y_gc(idx)) * (z_gc(idx) - z_grid(N0))
          
          g7 = (x_grid(L1) - x_gc(idx)) * (y_gc(idx) - y_grid(M0)) * (z_gc(idx) - z_grid(N0))
          g8 = (x_gc(idx) - x_grid(L0)) * (y_gc(idx) - y_grid(M0)) * (z_gc(idx) - z_grid(N0))

          fac = weight(idx)*Volume_scale
          E =   Energy_int(idx)
          Chi = Chi_int(idx)
             
          delta_f(L0,M0,N0,E,Chi) = delta_f(L0,M0,N0,E,Chi) + g1*fac  ;  gnrm(L0,M0,N0,E,Chi) = gnrm(L0,M0,N0,E,Chi) + (g1*Volume_scale)
          delta_f(L2,M0,N0,E,Chi) = delta_f(L2,M0,N0,E,Chi) + g2*fac  ;  gnrm(L2,M0,N0,E,Chi) = gnrm(L2,M0,N0,E,Chi) + (g2*Volume_scale)
          
          delta_f(L0,M2,N0,E,Chi) = delta_f(L0,M2,N0,E,Chi) + g3*fac  ;  gnrm(L0,M2,N0,E,Chi) = gnrm(L0,M2,N0,E,Chi) + (g3*Volume_scale)
          delta_f(L2,M2,N0,E,Chi) = delta_f(L2,M2,N0,E,Chi) + g4*fac  ;  gnrm(L2,M2,N0,E,Chi) = gnrm(L2,M2,N0,E,Chi) + (g4*Volume_scale)
          
          delta_f(L0,M0,N2,E,Chi) = delta_f(L0,M0,N2,E,Chi) + g5*fac  ;  gnrm(L0,M0,N2,E,Chi) = gnrm(L0,M0,N2,E,Chi) + (g5*Volume_scale)
          delta_f(L2,M0,N2,E,Chi) = delta_f(L2,M0,N2,E,Chi) + g6*fac  ;  gnrm(L2,M0,N2,E,Chi) = gnrm(L2,M0,N2,E,Chi) + (g6*Volume_scale)
          
          delta_f(L0,M2,N2,E,Chi) = delta_f(L0,M2,N2,E,Chi) + g7*fac  ;  gnrm(L0,M2,N2,E,Chi) = gnrm(L0,M2,N2,E,Chi) + (g7*Volume_scale)
          delta_f(L2,M2,N2,E,Chi) = delta_f(L2,M2,N2,E,Chi) + g8*fac  ;  gnrm(L2,M2,N2,E,Chi) = gnrm(L2,M2,N2,E,Chi) + (g8*Volume_scale)
          
       end do
    end do
    where (abs(gnrm) > epsilon(0.0)) 
       delta_f = delta_f/gnrm   
    end where

    if(velocity_space_diagnostics_on .AND. MOD(step,velocity_space_diagnostics_time)==0 .AND. MOD(collision_count,2)==0) then
       do i=1,Number_E
          do j=1,Number_Chi
             delta_f_dia(i,j) = delta_f(v_dia_x,v_dia_y,v_dia_z,i,j)
          end do
       end do
       call velocity_space_diagnostics(delta_f_dia)
    end if

!--- 2. use collision operator to find new delta_f(x,y,z,E,Chi) ---------------

!--- 3. find the weights by interpolation delta_f to the particle positions ---
    
    idx=0
    do ivperp=1,Nvperp
       Nvz = Nvz_Nvperp(ivperp)
       do k=1,Nvz

          idx = idx + 1
          E = Energy_int(idx)
          Chi = Chi_int(idx)

          L0 = (x_gc(idx)/delta_x)  ;  L1 = L0 + 1
          M0 = (y_gc(idx)/delta_y)  ;  M1 = M0 + 1
          N0 = (z_gc(idx)/delta_z)  ;  N1 = N0 + 1

          L2 = mod(L1, NX)  ;  M2 = mod(M1, NY) ;  N2 = mod(N1, NZ)

! changed 04/15/08
          weight(idx) = (1.-gamma)*weight(idx) + gamma*Volume_scale*( &
               (delta_f(L0,M0,N0,E,Chi)*(x_grid(L1) - x_gc(idx))*(y_grid(M1) - y_gc(idx))* (z_grid(N1) - z_gc(idx))) + &
               (delta_f(L2,M0,N0,E,Chi)*(x_gc(idx) - x_grid(L0))*(y_grid(M1) - y_gc(idx))* (z_grid(N1) - z_gc(idx))) + &

               (delta_f(L0,M2,N0,E,Chi)*(x_grid(L1) - x_gc(idx))*(y_gc(idx) - y_grid(M0))* (z_grid(N1) - z_gc(idx))) + &
               (delta_f(L2,M2,N0,E,Chi)*(x_gc(idx) - x_grid(L0))*(y_gc(idx) - y_grid(M0))* (z_grid(N1) - z_gc(idx))) + &

               (delta_f(L0,M0,N2,E,Chi)*(x_grid(L1) - x_gc(idx))*(y_grid(M1) - y_gc(idx))* (z_gc(idx) - z_grid(N0))) + &
               (delta_f(L2,M0,N2,E,Chi)*(x_gc(idx) - x_grid(L0))*(y_grid(M1) - y_gc(idx))* (z_gc(idx) - z_grid(N0))) + &

               (delta_f(L0,M2,N2,E,Chi)*(x_grid(L1) - x_gc(idx))*(y_gc(idx) - y_grid(M0))* (z_gc(idx) - z_grid(N0))) + &
               (delta_f(L2,M2,N2,E,Chi)*(x_gc(idx) - x_grid(L0))*(y_gc(idx) - y_grid(M0))* (z_gc(idx) - z_grid(N0))) )
       end do
    end do

    deallocate(delta_f)
    deallocate(delta_f_dia)

  end subroutine collision

!----------------------------------------------------------------------------------------------------------

  subroutine init_J0(vperp, J0)
    
    real(kind=pr), dimension(:), intent (in) :: vperp
    real(kind=pr), dimension(0:,0:,:), intent (out) :: J0
    
    real(kind=pr) :: kperp_vperp
    
    do j=1,Nvperp
       do iky = 0,Ny-1
          do ikx = 0,Nx/2
             kperp_vperp = vperp(j)*SQRT(k2_perp(ikx,iky))
             call j0_function(kperp_vperp, J0_init)
             J0(ikx,iky,j) = J0_init
          end do
       end do
    end do
    
  end subroutine init_J0
  
!----------------------------------------------------------------------------------------------------------
  subroutine init_particles
    
    integer :: i,j,l,m

    call random_number (wpos)  ;  wpos = (wpos - 0.5) * factor_delta
    call random_number (xpos)  ;  xpos = xpos * Lx
    call random_number (ypos)  ;  ypos = ypos * Ly
    call random_number (zpos)  ;  zpos = zpos * Lz

  end subroutine init_particles
!--------------------------------------------------------------------------------------------------------------------------------
  
  subroutine init_velocity_z

    integer :: j, k, l, idx
    real(kind=pr) :: vz_max

    idx=0
    do i = 1,Nvperp
       Nvz=Nvz_Nvperp(i)
       do j=1,Nvz
          idx=idx+1
          call random_number (velz(idx))
          vz_max = SQRT((Nvperp+1.)**2.-i**2.) * dvperp
          velz(idx) = (velz(idx)-0.5)*2.*vz_max
          vzw(idx) = exp(-(velz(idx)**2.)/2.)
       end do
    end do

  end subroutine init_velocity_z

!--------------------------------------------------------------------------------------------------------------------------------

  subroutine init_velocity_perp(vperp, dvperp)
    
    real(kind=pr), dimension(:), intent (out) :: vperp
    real(kind=pr), intent (in) :: dvperp

    do i=1,Nvperp
       vperp(i) = real(i)*dvperp  ! changed 12/07/07, we didn't have a point with vperp=0.before / changed again 12/11/07
    end do
    vperp_weight = vperp*exp(-(vperp**2.)/2.) 

  end subroutine init_velocity_perp

!--------------------------------------------------------------------------------------------------------------------------------
      
  subroutine j0_function(x_in, j0)
    
      ! A&S, p. 369, 9.4
    implicit none
    real(kind=pr), intent (in)  :: x_in
    real(kind=pr), intent (out) :: j0
    real(kind=pr), parameter, dimension (7) :: a = (/ 1.0000000, -2.2499997, 1.2656208, - 0.3163866, .0444479, -0.0039444, 0.0002100 /)
    real(kind=pr), parameter, dimension (7) :: b = (/  0.79788456, -0.00000770, -0.00552740, -0.00009512, 0.00137237, - 0.00072805,  0.00014476 /)
    real(kind=pr), parameter, dimension (7) :: c = (/ -0.78539816, -0.04166397, -0.00003954,  0.00262573, -0.00054125, -0.00029333,  0.00013558 /)
    real(kind=pr) :: y 
    
    if (x_in <= 3.0) then
       y = (x_in/3.0)**2
       j0 = a(1)+y*(a(2)+y*(a(3)+y*(a(4)+y*(a(5)+y*(a(6)+y*a(7))))))
    else
       y = 3.0/x_in
       j0 = (b(1)+y*(b(2)+y*(b(3)+y*(b(4)+y*(b(5)+y*(b(6)+y*b(7))))))) & 
            *cos(x_in+c(1)+y*(c(2)+y*(c(3)+y*(c(4)+y*(c(5)+y*(c(6)+y*c(7))))))) &
            /sqrt(x_in)
    end if
    
  end subroutine j0_function
!----------------------------------------------------------------------------------------------------------------------------------------------------

  subroutine gamma_n(x_LL,g0_LL)
      
    integer k
    real :: tolerance 
    parameter(tolerance=1.0e-16)
    
    real :: xk,xki,xkp1i,error0
    
    real(kind=pr), intent (in)  :: x_LL
    real(kind=pr), intent (out) :: g0_LL
    real :: tk0_LL, xb2_LL, xb2sq_LL
      
    if (x_LL == 0.) then
       g0_LL=1.
       return
    endif
    
    xb2_LL=.5*x_LL  
    xb2sq_LL=xb2_LL**2
    tk0_LL=exp(-x_LL)
    g0_LL=tk0_LL
      
    xk=1.
    
10  continue 
    
    xki=1./xk
    xkp1i=1./(xk+1.)
    tk0_LL=tk0_LL*xb2sq_LL*xki*xki
    g0_LL=g0_LL+tk0_LL
    xk=xk+1.
    error0=abs(tk0_LL/g0_LL)
    if (error0 > tolerance) goto 10
      
  end subroutine gamma_n

!----------------------------------------------------------------------------------------------------------------------------------------------

  subroutine read_inputs

!----------------------------------------------------------------------------------------------------------------------------
! this section reads in parameter values from an input file
    
    namelist / grid_par / Nx, Ny, Nz, Nvperp, Lx, Ly, Lz, Ncell, delta_t, nstep, dvperp
    namelist / dia_par / diagnostics_on, spectrum_diagnostics_on, energy_diagnostics_on, spectrum_write
    namelist / dia_new_par / diagnostics_new_on, diagnostics_1_on, diagnostics_2_on, diagnostics_3_on, diagnostics_4_on, nwrite
    namelist / dia_phi_par / phi2d_dia_on, phi3d_dia_on, phi2d_dia_time, phi3d_dia_time
    namelist / init_par / tau, charge, Ln, LT, nonlinear_on, ParkerLee, time_growth_off, nonlinear_phase, save_for_restart, init_restart, first_nonlinear
    namelist / dia_traj_par / diagnostics_traj_on, Ndisp, Nvbin, traj_dia_step
    namelist / collision_par / dE, N_Chi, collision_on, gamma, time_collision_on, collision_time
    namelist / vel_dia_par / v_dia_x, v_dia_y, v_dia_z, velocity_space_diagnostics_time, velocity_space_diagnostics_on

    call getarg(1, runname)
    in_str = trim(runname) // ".in"
    open(in_unit, file=in_str)
    
    read(in_unit,grid_par)
    read(in_unit,dia_par)
    read(in_unit,dia_new_par)
    read(in_unit,dia_phi_par)
    read(in_unit,init_par)
    read(in_unit,dia_traj_par)
    read(in_unit,collision_par)
    read(in_unit,vel_dia_par)
    
    close(in_unit)
!-------------------------------------------------------------------------------------------------------------------------------
    

  end subroutine read_inputs

  subroutine setup_IO
  
    phi2d_x_unit     = 16
    phi3d_x_unit     = 17
    energy_unit    = 18
    
    diagnostics_1_unit = 20    
    diagnostics_2_unit = 21
    diagnostics_3_unit = 22
    diagnostics_4_unit = 23

    diagnostics_traj_unit = 27    

    restart_unit = 28

    vel_diagnostics_unit = 29

    spectrum_k2_unit = 30
    spectrum_kx_unit = 31
    spectrum_ky_unit = 32

    phi_2_str = trim(runname) // "_phi2.out"
    rho_x_str = trim(runname) // "_rhox.out"
    rho_2_str = trim(runname) // "_rho2.out"
    log_phi_2_str = trim(runname) // "_log_phi2.out"
    
    spectrum_k2_str  = trim(runname) // ".kperp_spectrum"
    spectrum_kx_str  = trim(runname) // ".kx_spectrum"
    spectrum_ky_str  = trim(runname) // ".ky_spectrum"

    phi2d_x_str     = trim(runname) // ".phi2d"
    phi3d_x_str     = trim(runname) // ".phi3d"
    energy_str    = trim(runname) // ".energy"
    
    diagnostics_1_str = trim(runname) // ".dia_1"
    diagnostics_2_str = trim(runname) // ".dia_2"
    diagnostics_3_str = trim(runname) // ".dia_3"
    diagnostics_4_str = trim(runname) // ".dia_4"
    diagnostics_traj_str = trim(runname) // ".traj"

    restart_str = trim(runname) // ".restart"

    vel_diagnostics_str = trim(runname) //".vel_space"

    if(diagnostics_new_on) then
       if(diagnostics_1_on) open(unit=diagnostics_1_unit, file=diagnostics_1_str, status='replace')
       if(diagnostics_2_on) open(unit=diagnostics_2_unit, file=diagnostics_2_str, status='replace')
       if(diagnostics_3_on) then
          open(unit=diagnostics_3_unit, file=diagnostics_3_str, status='replace')
          write(diagnostics_3_unit,*) "# frequency, growth rate, kx, ky, kz"
       end if
       if(diagnostics_4_on) then
          open(unit=diagnostics_4_unit, file=diagnostics_4_str, status='replace')
          write(diagnostics_4_unit,*) "# time, sum_weights, heat_flux, phi^2"
       end if
    end if

    if(spectrum_diagnostics_on) then
       open(unit=spectrum_k2_unit, file=spectrum_k2_str, status='replace')
       write(spectrum_k2_unit,*) "# k_perp-Band, phi2(k_perp)"
       open(unit=spectrum_kx_unit, file=spectrum_kx_str, status='replace')
       write(spectrum_kx_unit,*) "# kx, <phi>(kx)"
       open(unit=spectrum_ky_unit, file=spectrum_ky_str, status='replace')
       write(spectrum_ky_unit,*) "# ky, <phi>(ky)"
    end if

    if(phi2d_dia_on) then
       open(unit=phi2d_x_unit, file=phi2d_x_str, status="replace")
       write(phi2d_x_unit,*) "# x, y, z, phi(x,y,z)"
    end if

    if(phi3d_dia_on) then
       open(unit=phi3d_x_unit, file=phi3d_x_str, status="replace")
       write(phi3d_x_unit,*) "# x, y, z, phi(x,y,z)"
    end if
             
    if(diagnostics_on) then
       if(energy_diagnostics_on) then
          open(unit=energy_unit, file=energy_str, status='replace')
          write(energy_unit,*) "ENERGY, ENSTROPHY, TIME"
       end if
    end if
    
  end subroutine setup_IO

  subroutine set_grid_parameters
    
    Msize(1) = Nx
    Msize(2) = Ny
    Msize(3) = Nz
    
    scale_x = 2.0*pi/Lx ! Lx is in units of rho ... scale_x = 2.0*pi/(Lx/rho)
    scale_y = 2.0*pi/Ly ! Ly is in units of rho ... scale_y = 2.0*pi/(Ly/rho)
    scale_z = 2.0*pi/Lz ! Lz is in units of a   ... scale_z = 2.0*pi/(Lz/a)
    
    delta_x = Lx/real(Nx)
    delta_y = Ly/real(Ny)
    delta_z = Lz/real(Nz)

    Volume_scale = 1./(delta_x*delta_y*delta_z)
    
    do i=0,Nx
       x_grid(i) = i*delta_x
    end do
    do i=0,Ny
       y_grid(i) = i*delta_y
    end do
    do i=0,Nz
       z_grid(i) = i*delta_z
    end do
    
    do ikx=0,Nx/2
       kx(ikx) = ikx*scale_x
    end do
    
    do iky=0,Ny/2
       ky(iky) = iky*scale_y
    end do
    
    do iky=(Ny/2)+1,Ny-1
       ky(iky) = -ky(Ny-iky)
    end do
    
    do ikz=0,Nz/2
       kz(ikz) = ikz*scale_z
    end do
    do ikz=(Nz/2)+1,Nz-1
       kz(ikz) = -kz(Nz-ikz)
    end do
    
    do iky=0,Ny-1
       do ikx=0,Nx/2
          k2_perp(ikx,iky) = kx(ikx)**2 + ky(iky)**2
       end do
    end do
    
    write (*,*) '#----------------------------'
    write (*,*) '# '
    write (*,*) '# Total number of particles: ',N
    write (*,*) '# '
    write (*,*) '# ky values: '
    do i=0,Ny-1
       write (*,*) '# ',ky(i)
    end do
    write (*,*) '# '
    write (*,*) '# kx values: '
    do i=0,Nx/2
       write (*,*) '# ',kx(i)
    end do
    write (*,*) '# '
    write (*,*) '# kz values: '
    do i=0,Nz-1
       write (*,*) '# ',kz(i)
    end do
    write (*,*) '#----------------------------'
    write (*,*)
    
  end subroutine set_grid_parameters

  subroutine allocate_memory

    allocate(kx(0:Nx/2))
    allocate(ky(0:Ny-1))
    allocate(kz(0:Nz-1))
    allocate(k2_perp(0:Nx/2,0:Ny-1))

    allocate(phi(0:Nx/2,0:Ny-1,0:Nz-1))    
    allocate(phi_old(0:Nx/2,0:Ny-1,0:Nz-1)) 

    if(diagnostics_traj_on) then
       allocate(xpos_true(1:Nvperp*Ndisp))
       allocate(ypos_true(1:Nvperp*Ndisp))
       allocate(zpos_true(1:Nvperp*Ndisp))
    end if

    allocate (p(15,N))
    
    xpos => p(x, :)  ;  ypos => p(y, :) ;  zpos => p(z, :)  ; wpos => p(wgt, :)
    xmid => p(xm,:)  ;  ymid => p(ym,:) ;  zmid => p(zm,:)  ; wmid => p(wgtm,:)
    velx => p(vx,:)  ;  vely => p(vy,:) !  FLR-corrected perpendicular drift velocity
    Epar => p(Ez,:)  ;  wdot => p(Ez,:) !  wdot intended to share memory with Epar
    velz => p(vz,:)  ;  vzw  => p(zw,:) !  Parallel velocity and integration weight in vz
    velp => p(vp,:)  ;  vpw  => p(pw,:) !  Perpendicular velocity and integration weight in vperp

    if (.not. nonlinear_on) then
       xmid => p(x,:)  ! no perp drifts if linear, so xmid = x
       ymid => p(y,:)  ! no perp drifts if linear, so ymid = y
    end if

    allocate (vperp(1:Nvperp))    
    allocate (vperp_weight(1:Nvperp))

    allocate (J0(0:Nx/2,0:Ny-1,1:Nvperp))
    
    allocate (x_grid(0:Nx))
    allocate (y_grid(0:Ny))
    allocate (z_grid(0:Nz))
    
    allocate(Energy_int(1:N))
    allocate(Chi_int(1:N))

  end subroutine allocate_memory

!-------------------------------------------------------------------------------------------

  subroutine spectrum_phi(phi_in)

    complex(kind=pr), dimension(0:,0:,0:), intent(in) :: phi_in
    complex(kind=pr), dimension(:,:), allocatable :: phi_k
    real(kind=pr), dimension(:), allocatable :: phi2_k, phi_kx, phi_ky
    integer, dimension(:), allocatable :: number
    integer :: ikx, iky, ikz, ik

    allocate(phi_k(0:Nx/2,0:Ny-1))
    allocate(phi2_k(1:Nx/2))
    allocate(number(1:Nx/2))
    allocate(phi_kx(0:Nx/2))
    allocate(phi_ky(0:Ny-1))

    phi_k     = 0.0
    phi2_k    = 0.0
    number    = 0 
    phi_kx    = 0.0
    phi_ky    = 0.0

    do ikz=0,Nz-1
       do iky=0,Ny-1
          do ikx=0,Nx/2
             phi_k(ikx,iky) = phi_k(ikx,iky) + phi_in(ikx,iky,ikz)
             phi_kx(ikx)    = phi_kx(ikx) + phi_in(ikx,iky,ikz)
             phi_ky(iky)    = phi_ky(iky) + phi_in(ikx,iky,ikz)
          end do
       end do
    end do

    do ik=1,Nx/2
       do ikx=0,ik
          do iky=0,ik
             if (ikx**2+iky**2 .le. ik**2 .and. ikx**2+iky**2 > (ik-1)**2) then
                phi2_k(ik) = phi2_k(ik) + phi_k(ikx,iky)*Conjg(phi_k(ikx,iky))
                number(ik) = number(ik) + 1
             end if
          end do
       end do
    end do
      
    do ik=1,Nx/2
       do ikx=0,ik
          do iky=Ny/2+1,Ny-1
             if (ikx**2+(Ny-iky)**2 .le. ik**2 .and. ikx**2+(Ny-iky)**2 > (ik-1)**2) then
                phi2_k(ik) = phi2_k(ik) + phi_k(ikx,iky)*Conjg(phi_k(ikx,iky))
                number(ik) = number(ik) + 1
             end if
          end do
       end do
    end do

    do ik=1,Nx/2
       phi2_k = phi2_k(ik)*(ik*2.*pi/Lx)/number(ik)
       write(spectrum_k2_unit,*) ik*2.*pi/Lx, phi2_k(ik)
       write(spectrum_kx_unit,*) ik*2.*pi/Lx, phi_kx(ik)
    end do

    do ik=0,Ny/2
       write(spectrum_ky_unit,*) ik*2.*pi/Ly, phi_ky(ik)
    end do
    do ik=Ny/2+1,Ny-1
       write(spectrum_ky_unit,*) (-Ny+ik)*(2.*pi/Ly), phi_ky(ik)
    end do

    deallocate(phi_k, phi2_k, phi_kx, phi_ky, number)

  end subroutine spectrum_phi


end program PIC_GK








