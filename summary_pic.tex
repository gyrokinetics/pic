
\documentclass[12pt]{article}
\usepackage{graphicx}

\title{Notes to PIC-code}
\author{Ingmar Broemstrup}
\date{\today}                                         
\begin{document}
\maketitle

This is supposed to give a short summary of the code as it stands today. The code solves the electrostatic version of the gyrokinetic equation in a slab with no collisions and periodic boundary conditions. We have a strong background magnetic field which defines the parallel direction for this system. 

In the gyrokinetic equation the particle distribution function is divided into a slowly evolving (space and time) equilibrium $f_{0}$ and a perturbed part $\delta f (x,y,z,v_{\parallel},v_{\perp},t)$ that varies fast in space and on a medium time-scale. The brackets $< \cdot >$ symbolize a gyroaverage. This is an average over the fast gyration motion that particles are describing around the magnetic field lines. The radius of this motion for an individual particle is directly proportional to the particle's value of $v_{\perp}$. 
\begin{eqnarray}
\frac{\partial}{\partial t} <\delta f> + <\mathbf{v_{E}}> \cdot \nabla <\delta f> + v_{\parallel} \nabla_{\parallel} <\delta f>  = - <\mathbf{v_{E}}> \cdot \nabla f_{0} - \frac{q}{m} E_{\parallel} \frac{\partial}{\partial v_{\parallel}} f_{0}
\end{eqnarray}
Writing the total time derivative of $\delta f$ allows us to find the characteristics of this PDE:
\begin{eqnarray}
\frac{d}{dt} <\delta f> =  \frac{\partial}{\partial t} <\delta f> + \left( \frac{d}{dt} \mathbf{x} \right) \cdot \nabla <\delta f> + \left( \frac{d}{dt} v_{\perp} \right) \frac{\partial}{\partial v_{\perp}} <\delta f> + \left( \frac{d}{dt} v_{\parallel} \right) \frac{\partial}{\partial v_{\parallel}} <\delta f>
\end{eqnarray}
So the characteristics for this system are:
\begin{eqnarray}
\label{perpendicular_drift}
\frac{d}{dt} \mathbf{x_{\perp}} = \ < \mathbf{v_{E}} > \\
\frac{d}{dt} x_{\parallel} = v_{\parallel} \\
\frac{d}{dt} v_{\parallel} = 0 \\
\frac{d}{dt} v_{\perp} = 0 
\end{eqnarray}
Along the characteristics we are left with the following equation:
\begin{eqnarray}
\frac{d}{dt} < \delta f > = - <\mathbf{v_{E}}> \cdot \nabla f_{0} - \frac{q}{m} E_{\parallel} \frac{\partial}{\partial v_{\parallel}} f_{0}
\end{eqnarray}
This is an ODE since we know the form of $f_{0}$ and can rewrite the RHS without derivatives.
So all we need to do is to solve the three ODE's Eq.(3),(4) and (7). 
Therefore we define the weight of particle $i$ as :
\begin{eqnarray}
w_{i} \equiv \frac{< \delta f>}{f} |_{\mathbf{x_{i}},v_{\perp_{i}},v_{\parallel_{i}}}
\end{eqnarray}
We use the particle weight when we solve for $<\delta f>$ in the code (the P2V step) by bilinear interpolation:
\begin{eqnarray}
\delta f(x,y,z,v_{\parallel}, v_{\perp}) = \sum_{i=1}^{N} w_{i} \delta(\mathbf{x} - \mathbf{x_{i}}) \delta(v_{\parallel} - v_{\parallel_{i}}) \delta(v_{\parallel} - v_{\parallel_{i}} )
\end{eqnarray}
The time evoultion of the particle weights is given by Eq.(7)
\begin{eqnarray}
\frac{d}{dt} w_{i} = - (1-w_{i}) \left( <\mathbf{v_{E}}> \cdot \frac{\nabla f_{0}}{f_{0}} + \frac{q}{m} E_{\parallel} \frac{\frac{\partial}{\partial v_{\parallel}}f_{0}}{f_{0}}  \right)
\end{eqnarray}
The equilibrium $f_{0}$ is described by a Maxwellian of the form:
\begin{eqnarray}
f_{0} = \frac{n(x)}{\left( \sqrt{2\pi} \sqrt{\frac{T(x)}{m_{i}}} \right)^{3}} \ exp\left( - \frac{v^{2}}{2 \frac{T(x)}{m_{i}}} \right)
\label{Eqf0}
\end{eqnarray}
with $T(x) = T_{0} e^{-\frac{x}{L_{T}}}$ and $n(x) = n_{0} e^{-\frac{x}{L_{n}}}$. \\
The time evolution of the particle weights becomes in the normalized units of the code:
\begin{eqnarray}
\frac{d}{dt} w_{i} = - (1 - w_{i}) \left( \hat{\mathbf{x}} \cdot <\mathbf{v_{E}} > \left( \frac{3 - v^{2}}{2 L_{T}} - \frac{1}{L_{n}} \right) - v_{\parallel} \nabla_{\parallel} <\phi> \right)
\end{eqnarray}

Probably I should have given this definition before. The E$\times$B velocity for which I use the symbol $\mathbf{v_{E}}$ in these notes is defined in the normalized units of the code by:
\begin{eqnarray}
<\mathbf{v_{E}}> = \hat{\mathbf{z}} \times \nabla <\phi>
\end{eqnarray}

So for the characteristics as well as for the time evolution of the particle weights we need to solve for $\phi$. If you are mostly interested in how the code works you can skip the derivation and go straight to Eq.(\ref{Poisson}).
\\
Before we explain how we solve the Poisson's Equation we need to give a complete definition for the way how we take a gyro-average.  
Actually we are using two different processes for taking those ring averages:
\\

1. Average over a ring centered at the gyrocenter position $\mathbf{R}$ with radius $\rho_{i} = v_{\perp}/\Omega$: 
\begin{eqnarray}
< A(\mathbf{r},\mathbf{v},t) >_{\mathbf{R}} = \frac{1}{2\pi} \int_{0}^{2\pi} A(\mathbf{R} - \frac{\mathbf{v} \times \hat{\mathbf{b}}}{\Omega},\mathbf{v},t) d\theta
\end{eqnarray}
The integration is done while keeping the gyro-center position $\mathbf{R} = \mathbf{r} + \frac{\mathbf{v} \times \hat{\mathbf{b}}}{\Omega}$, $v_{\perp}$ and $v_{\parallel}$ fixed. 
\\

2. Average over a ring centered at the particle position $\mathbf{r}$ :
\begin{eqnarray}
<A(\mathbf{R},\mathbf{v},t)>_{\mathbf{r}} = \frac{1}{2\pi} \int_{0}^{2\pi} A(\mathbf{r}+\frac{\mathbf{v} \times \hat{\mathbf{b}}} {\Omega},\mathbf{v},t) d\theta
\end{eqnarray}
The integration is done while keeping $\mathbf{r}$ fixed with a radius of $\phi_{i}=v_{\perp}/\Omega$.
\\

The Poisson equation is given by :
\begin{eqnarray}
\nabla^{2} \phi = - \frac{1}{\epsilon_{0}} \left( Zen_{i} - en_{e} \right)
\end{eqnarray}
When $k_{\perp}^{-1}$ is long compared to the Debye length (scale over which charge carriers screen out electric fields in plasmas) one can drop the left hand side of Poisson's equation and get the quasineutrality condition:
\begin{eqnarray}
Ze n_{i} = e n_{e}
\end{eqnarray}

$n_{i}$ is given by: 
\begin{eqnarray}
n_{i} = \int <\delta f>_{\mathbf{r}} \ d^{3}v
\end{eqnarray}

\begin{eqnarray}
\delta f = h - \frac{Z e}{T_{i}} \phi f_{0}
\end{eqnarray}

\begin{eqnarray}
<\delta f>_{\mathbf{R}} = h - \frac{Ze}{T_{i}} f_{0} <\phi>_{\mathbf{R}} 
\end{eqnarray}

\begin{eqnarray}
\delta f = <\delta f>_{\mathbf{R}} + \frac{Ze}{T_{i}} f_{0} \left( <\phi>_{\mathbf{R}} - \phi \right)
\end{eqnarray}

We assume adiabatic electrons so $n_{e}$ is given by:

\begin{eqnarray}
n_{e} = \frac{e \phi}{T_{e}} n_{0}
\end{eqnarray}

We rewrite the quasi-neutrality condition as:

\begin{eqnarray}
\frac{e}{T_{e}} \phi n_{0} = Z \int < \left( <\delta f>_{\mathbf{R}} + \frac{Ze}{T_{i}} f_{0} \left( <\phi>_{\mathbf{R}} - \phi \right) \right) >_{\mathbf{r}} d^{3}v
\end{eqnarray}
Note $\rho = \frac{v_{T}}{\Omega}$ and $v_{T} = \sqrt{\frac{T_{i}}{m_{i}}}$ and that we are working in Fourier Space 
$\Leftrightarrow$ 
\begin{eqnarray}
\frac{e}{T_{e}} \phi n_{0} = Z \int J_{0}\left( \frac{k_{\perp} v_{\perp}}{\Omega} \right) <\delta f>_{\mathbf{R}} d^{3}v + \frac{Z^{2}e}{T_{i}} \left( \Gamma_{0} \left( k^{2}_{\perp} \rho^{2} \right) - 1\right) n_{0} \phi
\end{eqnarray}
$\Leftrightarrow$
\begin{eqnarray}
n_{0} e \left( \frac{1}{Z T_{e}} + \frac{Z}{T_{i}} - \frac{Z}{T_{i}} \Gamma_{0} \left( k^{2}_{\perp} \rho^{2} \right) \right) \phi = \int J_{0} \left( \frac{k_{\perp} v_{\perp}}{\Omega} \right) <\delta f>_{\mathbf{R}} d^{3}v
\label{Eq3}
\end{eqnarray}

We are now rewriting the last equation in terms of normalized and therefore dimensionless quantities that are used in the code. A subscript N marks a normalized quantity.

\begin{eqnarray}
\mbox{normalized electrostatic potential: \ }  \phi_{N} = \frac{A e}{\rho T_{i}} \phi \\ 
\mbox{normalized disturbed part of the distribution function: \ } \delta f_{N} = \frac{A}{\rho} \delta f \\
\mbox{normalized time: \ } t_{N} = \frac{v_{T}}{A} t \\
\mbox{normalized length in perpendicular direction: \ } l_{\perp_{N}} = \frac{1}{\rho} l \Rightarrow k_{\perp_{N}} =  \rho k_{\perp} \\
\mbox{normalized length in parallel direction: \ } l_{\parallel_{N}} = \frac{1}{A} l \Rightarrow k_{\parallel} =   A k_{\parallel} \\
\mbox{normalized velocities: \ } v_{\perp_{N}} = \frac{v_{\perp}}{v_{T}} , v_{\parallel_{N}} = \frac{v_{\parallel}}{v_{T}}
\end{eqnarray}
Note: $v^{2}_{T} = \frac{T_{i}}{m_{i}}$; $\Omega = \frac{ZeB}{m_{i}}$; $\rho =\frac{v_{T}}{\Omega}$; $\rho_{i} = \frac{v_{\perp}}{\Omega}$ \\

So we can rewrite Eq.~(\ref{Eq3}) as:

\begin{eqnarray}
n_{0} \left( \frac{T_{i}}{T_{e} Z} + Z - Z \Gamma_{0} \left( k^{2}_{\perp_{N}} \right) \right)  \phi_{N} = \int J_{0} \left( k_{\perp_{N}} v_{\perp_{N}} \right) <\delta f_{N}>_{\mathbf{R}}  d^{3}v
\end{eqnarray}

The parameter $\tau$ is defined as the ratio of ion to electron temperature. $\tau \equiv \frac{T_{i}}{T_{e}}$. $\Leftrightarrow$

\begin{eqnarray}
\phi_{N}= \frac{n_{0}}{\frac{\tau}{Z} + Z - Z \  \Gamma_{0} \left( k^{2}_{\perp_{N}} \right) } \int J_{0} \left( k_{\perp_{N}} v_{\perp_{N}} \right) <\delta f>_{\mathbf{R}} d^{3}v
\label{Poisson}
\end{eqnarray}

Eq.(\ref{Poisson}) is solved in the code in the subroutine \textit{calculate\_phi}. This is a very central subroutine for the code and includes the P2V operation. Since the velocity part of the distribution function is integrated out we don't solve for the fully 5-dimensional distribution function $\delta f(x,y,z,v_{\perp},v_{\parallel})$. Instead we use the following approach. 
The $v_{\perp}$ values of the particles don't get updated in the code. We initialize the particles on a grid in $v_{\perp}$ and have $Nv_{\perp}$ grid-points in the positive part of the $v_{\perp}$-space. We order the particles in chunks of $\frac{N}{Nv_{\perp}}$ particles that have all the same value of $v_{\perp}$. So we solve Eq.(\ref{Poisson}) for one $v_{\perp}$-value at a time. For $v_{\parallel}$ we are using a different approach. The code does not have a grid in the $v_{\parallel}$-dimension and we initialize the values of $v_{\parallel}$ randomly drawn from a uniform distribution. So while solving for $\phi$ we use a Monte Carlo integration scheme to integrate out the $v_{\parallel}$-dependency. There is an additional complication to the velocity part of the distribution. We assume that the distribution is Maxwellian in both velocity dimensions. 
Therefore each particle is given a velocity weight according to its values for $v_{\perp}$ and $v_{\parallel}$: 
\begin{eqnarray}
w_{v_{\perp_{i}}} = \frac{1}{\sqrt{2\pi}}e^{-\frac{v_{\perp_{i}}^{2}}{2}} \\
w_{v_{\parallel_{i}}} =\frac{1}{\sqrt{2\pi}} e^{-\frac{v_{\parallel_{i}}^{2}}{2}}
\end{eqnarray}
The actual form of the velocity weights in the code is a little bit different. Since we work in cylindrical coordinates we need to include the Jacobian, $dv^{3} = 2 \pi v_{\perp} dv_{\perp} dv_{\parallel}$, in Eq.(\ref{Poisson}). The weights in the code, called \textit{vz\_weight} and \textit{vperp\_weight}, are set to be:
\begin{eqnarray}
w_{v_{\perp_{i}}} = v_{\perp_{i}} e^{-\frac{v_{\perp_{i}}^{2}}{2}} \ \ \ \ v_{\perp} \in [ \Delta v_{\perp} , Nv_{\perp} \times \Delta v_{\perp}  ]  \\
w_{v_{\parallel_{i}}} = e^{-\frac{v_{\parallel_{i}}^{2}}{2}} \ \ \ \ v_{\parallel} \in ( -\frac{1}{2} Nv_{\parallel} \times \Delta v_{\parallel} , \frac{1}{2} Nv_{\parallel} \times \Delta v_{\parallel} )
\end{eqnarray}

Before we explain more about how the code works we list the main parameters that one can set in the INPUT file before running the code. 
\\

1) physical grid: \\
\begin{displaymath}
\left. %
\begin{array}  {@{} l@ {\quad } ccrr@ {}}
	\textrm {a}) & Nx & = & \mbox{integer, number grid points in x-direction} \\
	\textrm {b}) & Ny & = & \mbox{integer, number grid points in y-direction} \\
	\textrm {c}) & Nz & = & \mbox{integer, number grid points in z-direction} \\
	\\
	\textrm {d}) & Lx & = & \mbox{real, box length in units of $\rho$} \\
	\textrm {e}) & Ly & = & \mbox{real, box length in units of $\rho$} \\
	\textrm {f}) & Lz & = & \mbox{real, box length in units of $A$} \\	
\end{array} %
\right \} \textrm{physical space}
\end{displaymath}
\\

2) velocity grid: \\
\\
a) $Nvperp \ = \ $ integer, number grid points in $v_{\perp}$-direction \\
b) $dvperp \ = \ $ real, distance between grid points  \\
\begin{displaymath}
\left. %
\begin{array}  {@{} l@ {\quad } ccrr@ {}}
	\textrm {c}) & Nvz & = & \mbox{integer, number grid points in parallel z-direction} \\
	\textrm {d}) & dvz & = & \mbox{real, sets with $Nvperp$ the maximum value of $v_{\parallel}$} \\
\end{array}
\right \} \textrm{$Nvz \times dvz =$Max. $v_{\parallel}$}
\end{displaymath}
\\

3) number of particles (in the code called $N$) \\
\\
$Ncell$, integer \\
The number of particles is set indirectly in the input file. We don't set $N$ but instead $N$ is defined as the product of $Nvperp, Nvz$ and $Ncell$. \\
\begin{equation}
N = Nvperp \times Nvz \times Ncell
\end{equation}
$Ncell$ is the number of particles that are (on average) in a velocity space element.\\
\\

4) time\\
$delta\_t :$ real, fixed time step in the code. Has to be changed to be adaptive\\
$nstep :$ integer, number of timesteps the code runs.\\
\\

5) additional parameters \\
$tau :$ real, the ratio of ion to electron temperature. ($\tau$ in Eq.(\ref{Poisson})) \\
$charge :$ integer, the number of charges that one ion carries. ($Z$ In Eq.(\ref{Poisson})) \\
$Ln :$ real, gradient scale length of the background density.\\ 
$LT :$ real, gradient scale length of the background ion temperature. \\
$nonlinear\_on :$ logical, turns on the nonlinear term. When the term is turned we don't update the particle position in the perpendicular direction and the RHS of Eq.(\ref{perpendicular_drift}) is zero. 
\\
\\
There are more parameter that are dealing with the diagnostics and I am not going to reference here to them.
\\
\\
Back to the code and how it works. \\
Recall the four equations that we need to solve are:
\begin{eqnarray*}
\begin{array}  {@{} l@ {\quad } ccrr@ {}}
	\textrm{a}) \ \ \frac{d}{dt} \mathbf{x_{\perp}} = \ < \mathbf{v_{E}} > \\ \\
	\textrm{b}) \ \ \frac{d}{dt} x_{\parallel} = v_{\parallel} \\ \\
	\textrm{c}) \ \  \frac{d}{dt} w_{i} = - (1 - w_{i}) \left( \hat{\mathbf{x}} \cdot <\mathbf{v_{E}} > \left( \frac{3 - v^{2}}{2 L_{T}} - \frac{1}{L_{n}} \right) - v_{\parallel} \nabla_{\parallel} <\phi> \right)  \\ \\
	\textrm{d}) \ \ \phi_{N}= \frac{n_{0}}{\frac{\tau}{Z} + Z - Z \  \Gamma_{0} ( k^{2}_{\perp_{N}} ) } \int J_{0} \left( k_{\perp_{N}} v_{\perp_{N}} \right) <\delta f>_{\mathbf{R}} d^{3}v
\end{array}
\end{eqnarray*}

Above we described the subroutine $calculate\_phi$ that solves equation d). Equation b) is trivial since $v_{\parallel}$ doesn't get updated. For equation a) and c) we need to know the value of the $E\times B$-velocity at the position of the gyration center of each particle. Recall: $<\mathbf{v_{E}}> = \hat{\mathbf{z}} \times \nabla <\phi>$. \\
Therefore another essential subroutine in the code is the subroutine called $calculate\_electric\_field$ that returns the value of the electric field at the particle positions. This subroutine takes the derivative of $\phi$ in Fourier Space, then takes an inverse Fourier Transformation to real space and uses a inverse bilinear interpolation to find the value of the $E\times B$-drift-velocities at the particle positions (V2P). 
Solving the ODE's of equation a) and c) is stright forward and we use a "predictor corrector method" that is second order accurate. 
\\
\end{document}  